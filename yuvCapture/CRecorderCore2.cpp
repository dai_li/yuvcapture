//#include "stdafx.h"
#include "CRecorderCore2.h"

typedef enum
{
	WEAVE_DEINTERLACE,  /*merge or weave */
	DISCARD_DEINTERLACE,  /*discard*/
	BOB_DEINTERLACE
}DeInterlaceType;


char g_yuvSavePath[255];
FILE *g_fp=nullptr;
FILE *g_fp_text = nullptr;
bool g_bSaveYUV = false;
CRecorderCore2::CRecorderCore2()
{

	InitializeCriticalSection(&m_Lock);
	if (g_bSaveYUV)
	{
		sprintf_s(g_yuvSavePath, "D:\\Data\\");
		CreateDirectoryA(g_yuvSavePath, NULL);

		char filepath[255];
		sprintf_s(filepath, "%s\\global.yuv", g_yuvSavePath);
		fopen_s(&g_fp, filepath, "wb");

		sprintf_s(filepath, "%s\\global.txt", g_yuvSavePath);
		fopen_s(&g_fp_text, filepath, "w");
	}

}


CRecorderCore2::~CRecorderCore2()
{
}

bool CRecorderCore2::isSupportedFormat()
{

	if (m_displayMode == bmdModePAL)
		return true;

	if (m_displayMode == bmdModeNTSC)
		return true;

	if (m_displayMode == bmdModeHD720p50)
		return true;

	if (m_displayMode == bmdModeHD720p5994)
		return true;

	if (m_displayMode == bmdModeHD720p60)
		return true;

	if (m_displayMode == bmdModeHD1080i50)
		return true;

	if (m_displayMode == bmdModeHD1080i5994)
		return true;

	if (m_displayMode == bmdModeHD1080i6000)
		return true;

	if (m_displayMode == bmdModeHD1080p2398)
		return true;

	if (m_displayMode == bmdModeHD1080p24)
		return true;

	return false;
}

HRESULT STDMETHODCALLTYPE CRecorderCore2::VideoInputFormatChanged(BMDVideoInputFormatChangedEvents notificationEvents, IDeckLinkDisplayMode *newDisplayMode, BMDDetectedVideoInputFormatFlags detectedSignalFlags)
{
	return 0;
}

bool CRecorderCore2::isValidVideo(IDeckLinkVideoInputFrame * _videoFrame)
{
	if ((_videoFrame->GetFlags() & bmdFrameHasNoInputSource) != bmdFrameHasNoInputSource)
		return true;
	return false;
}
VideoModeInfo  CRecorderCore2::GetTargetModeInfo(int modeIn)
{
	VideoModeInfo  modeInfo;
	VideoFormat_ENUM & modeOut = modeInfo.mode;
	int& pic_w = modeInfo.width;
	int& pic_h = modeInfo.height;
	bool& bob = modeInfo.BobNeeded;
	bool& drop = modeInfo.DropframeNeeded;
	int&  timecodeformat = modeInfo.timecodeformat;

	switch (modeIn)
	{
	case bmdModeHD1080i50:
	{
		pic_w = PIC1080WIDTH;
		pic_h = PIC1080HEIGHT;
		timecodeformat = 25;
		if (m_scaleTo == 1)  //720P
		{
			pic_w = PIC720WIDTH;
			pic_h = PIC720HEIGHT;
			if (m_desinterlaceType == BOB_DEINTERLACE)
			{
				bob = true;
				timecodeformat = 50;
				modeOut = ENCODER_VIDEO_FORMAT_720p_50;
			}
			else
				modeOut = ENCODER_VIDEO_FORMAT_720p_25;
		}
		else if (m_scaleTo == 2) //sd
		{
			pic_w = PICSDWIDTH;
			pic_h = PICSDPALHEIGHT;
			if (m_sd_ratio == Mode_16_9)
			{
				pic_h = 404;
				pic_w = 720;
			}
			modeOut = ENCODER_VIDEO_FORMAT_SD_PAL;
			if (m_desinterlaceType == BOB_DEINTERLACE)
			{
				bob = true;
				timecodeformat = 50;
			}
		}
		else //1080i
		{
			if (m_desinterlaceType == BOB_DEINTERLACE)
			{
				modeOut = ENCODER_VIDEO_FORMAT_1080i_50;
			}
			else
				modeOut = ENCODER_VIDEO_FORMAT_1080p_25;
		}
		break;
	}
	case bmdModeHD1080i5994:
	{
		pic_w = PIC1080WIDTH;
		pic_h = PIC1080HEIGHT;
		timecodeformat = 29;
		if (m_scaleTo == 1)  //720P
		{
			pic_w = PIC720WIDTH;
			pic_h = PIC720HEIGHT;
			if (m_desinterlaceType == BOB_DEINTERLACE)
			{
				bob = true;
				timecodeformat = 59;
				modeOut = ENCODER_VIDEO_FORMAT_720p_5994;
			}
			else
			{
				modeOut = ENCODER_VIDEO_FORMAT_720p_2997;
			}
		}
		else if (m_scaleTo == 2) //sd
		{
			pic_w = PICSDWIDTH;
			pic_h = PICSDNTSCHEIGHT;
			if (m_sd_ratio == Mode_16_9)
			{
				pic_h = 404;
				pic_w = 720;
			}
			modeOut = ENCODER_VIDEO_FORMAT_SD_NTSC;
			if (m_desinterlaceType == BOB_DEINTERLACE)
			{
				bob = true;
				timecodeformat = 59;
			}
			else
				timecodeformat = 29;
		}
		else //auto 1080i
		{
			if (m_desinterlaceType == BOB_DEINTERLACE)
			{
				modeOut = ENCODER_VIDEO_FORMAT_1080i_5994;
			}
			else
			{
				modeOut = ENCODER_VIDEO_FORMAT_1080p_2997;
			}
		}
		break;
	}
	case bmdModeHD720p50:
	{
		pic_w = PIC720WIDTH;
		pic_h = PIC720HEIGHT;
		modeOut = ENCODER_VIDEO_FORMAT_720p_50;
		timecodeformat = 50;
		if (m_scaleTo == 2) //sd
		{
			pic_w = PICSDWIDTH;
			pic_h = PICSDPALHEIGHT;
			if (m_sd_ratio == Mode_16_9)
			{
				pic_h = 404;
				pic_w = 720;
			}
			modeOut = ENCODER_VIDEO_FORMAT_SD_PAL;
			if (m_desinterlaceType == DISCARD_DEINTERLACE)
			{
				timecodeformat = 25;
				drop = true;
			}
		}
		else
		{
			if (m_desinterlaceType == DISCARD_DEINTERLACE)
			{
				timecodeformat = 25;
				{
					modeOut = ENCODER_VIDEO_FORMAT_720p_25;
					drop = true;
				}
			}
		}
		break;
	}
	case bmdModeHD720p5994:
	{
		pic_w = PIC720WIDTH;
		pic_h = PIC720HEIGHT;
		modeOut = ENCODER_VIDEO_FORMAT_720p_5994;
		timecodeformat = 59;
		if (m_scaleTo == 2) //sd
		{
			pic_w = PICSDWIDTH;
			pic_h = PICSDNTSCHEIGHT;
			if (m_sd_ratio == Mode_16_9)
			{
				pic_h = 404;
				pic_w = 720;
			}
			if (m_desinterlaceType == DISCARD_DEINTERLACE)
			{
				timecodeformat = 29;
				drop = true;
			}
			modeOut = ENCODER_VIDEO_FORMAT_SD_NTSC;
		}
		else
		{
			if (m_desinterlaceType == DISCARD_DEINTERLACE)
			{
				modeOut = ENCODER_VIDEO_FORMAT_720p_2997;
				timecodeformat = 29;
				drop = true;
			}
		}
		break;
	}
	case bmdModeHD720p60:
	{
		pic_w = PIC720WIDTH;
		pic_h = PIC720HEIGHT;
		modeOut = ENCODER_VIDEO_FORMAT_720p_60;
		timecodeformat = 60;
		if (m_scaleTo == 2) //sd
		{
			pic_w = PICSDWIDTH;
			pic_h = PICSDNTSCHEIGHT;
			if (m_sd_ratio == Mode_16_9)
			{
				pic_h = 404;
				pic_w = 720;
			}
			if (m_desinterlaceType == DISCARD_DEINTERLACE)
			{
				drop = true;
				timecodeformat = 30;
			}
			modeOut = ENCODER_VIDEO_FORMAT_SD_NTSC;
		}
		else
		{
			if (m_desinterlaceType == DISCARD_DEINTERLACE)
			{
				drop = true;
				timecodeformat = 30;
				modeOut = ENCODER_VIDEO_FORMAT_720p_30;
			}
		}
		break;
	}
	case bmdModeHD1080i6000:
	{
		pic_w = PIC1080WIDTH;
		pic_h = PIC1080HEIGHT;
		modeOut = ENCODER_VIDEO_FORMAT_1080i_60;
		timecodeformat = 30;
		if (m_scaleTo == 1)  //720P
		{
			pic_w = PIC720WIDTH;
			pic_h = PIC720HEIGHT;
			if (m_desinterlaceType == BOB_DEINTERLACE)
			{
				bob = true;
				timecodeformat = 60;
				modeOut = ENCODER_VIDEO_FORMAT_720p_60;
			}
			else
				modeOut = ENCODER_VIDEO_FORMAT_720p_30;
		}
		else if (m_scaleTo == 2) //sd
		{
			pic_w = PICSDWIDTH;
			pic_h = PICSDNTSCHEIGHT;
			if (m_sd_ratio == Mode_16_9)
			{
				pic_h = 404;
				pic_w = 720;
			}
			if (m_desinterlaceType == BOB_DEINTERLACE)
			{
				bob = true;
				timecodeformat = 60;
			}
			modeOut = ENCODER_VIDEO_FORMAT_SD_NTSC;
		}
		else
		{
			if (m_desinterlaceType != BOB_DEINTERLACE)
				modeOut = ENCODER_VIDEO_FORMAT_1080p_30;
		}
		break;
	}

	case bmdModeHD1080p30:
	{
		pic_w = PIC1080WIDTH;
		pic_h = PIC1080HEIGHT;
		modeOut = ENCODER_VIDEO_FORMAT_1080p_30;
		timecodeformat = 30;
		if (m_scaleTo == 1)  //720P
		{
			pic_w = PIC720WIDTH;
			pic_h = PIC720HEIGHT;
			if (m_desinterlaceType == BOB_DEINTERLACE)
			{
				bob = true;
				timecodeformat = 60;
				modeOut = ENCODER_VIDEO_FORMAT_720p_60;
			}
			else
				modeOut = ENCODER_VIDEO_FORMAT_720p_30;
		}
		else if (m_scaleTo == 2) //sd
		{
			pic_w = PICSDWIDTH;
			pic_h = PICSDNTSCHEIGHT;
			if (m_sd_ratio == Mode_16_9)
			{
				pic_h = 404;
				pic_w = 720;
			}
			if (m_desinterlaceType == BOB_DEINTERLACE)
			{
				bob = true;
				timecodeformat = 60;
			}
			modeOut = ENCODER_VIDEO_FORMAT_SD_NTSC;
		}
		else
		{
			modeOut = ENCODER_VIDEO_FORMAT_1080p_30;
		}
		break;
	}
	case bmdModeHD1080p2997:
	{
		pic_w = PIC1080WIDTH;
		pic_h = PIC1080HEIGHT;
		modeOut = ENCODER_VIDEO_FORMAT_1080p_2997;
		timecodeformat = 29;
		if (m_scaleTo == 1)  //720P
		{
			pic_w = PIC720WIDTH;
			pic_h = PIC720HEIGHT;

			if (m_desinterlaceType == BOB_DEINTERLACE)
			{
				bob = true;
				timecodeformat = 59;
				modeOut = ENCODER_VIDEO_FORMAT_720p_5994;
			}
		}
		else if (m_scaleTo == 2) //sd
		{
			pic_w = PICSDWIDTH;
			pic_h = PICSDNTSCHEIGHT;
			if (m_sd_ratio == Mode_16_9)
			{
				pic_h = 404;
				pic_w = 720;
			}
			if (m_desinterlaceType == BOB_DEINTERLACE)
			{
				timecodeformat = 59;
				bob = true;
			}
			modeOut = ENCODER_VIDEO_FORMAT_SD_NTSC;
		}

		break;
	}
	case bmdModeHD1080p25:
	{
		pic_w = PIC1080WIDTH;
		pic_h = PIC1080HEIGHT;
		modeOut = ENCODER_VIDEO_FORMAT_1080p_25;
		timecodeformat = 25;
		if (m_scaleTo == 1)  //720P
		{
			pic_w = PIC720WIDTH;
			pic_h = PIC720HEIGHT;
			modeOut = ENCODER_VIDEO_FORMAT_720p_25;
		}
		else if (m_scaleTo == 2) //sd
		{
			pic_w = PICSDWIDTH;
			pic_h = PICSDPALHEIGHT;
			if (m_sd_ratio == Mode_16_9)
			{
				pic_h = 404;
				pic_w = 720;
			}
			if (m_desinterlaceType == BOB_DEINTERLACE)
			{
				bob = true;
				timecodeformat = 50;
			}
			modeOut = ENCODER_VIDEO_FORMAT_SD_PAL;
		}

		break;
	}
	case bmdModeHD1080p24:
	{
		pic_w = PIC1080WIDTH;
		pic_h = PIC1080HEIGHT;
		modeOut = ENCODER_VIDEO_FORMAT_1080p_24;
		timecodeformat = 24;
		if (m_scaleTo == 1)  //720P
		{
			pic_w = PIC720WIDTH;
			pic_h = PIC720HEIGHT;
			modeOut = ENCODER_VIDEO_FORMAT_720p_24;
		}
		break;
	}
	case bmdModeHD1080p2398:
	{
		pic_w = PIC1080WIDTH;
		pic_h = PIC1080HEIGHT;
		modeOut = ENCODER_VIDEO_FORMAT_1080p_2398;
		timecodeformat = 23;
		if (m_scaleTo == 1)  //sd
		{
			pic_w = PIC720WIDTH;
			pic_h = PIC720HEIGHT;
			modeOut = ENCODER_VIDEO_FORMAT_720p_2398;
		}

		break;
	}
	case bmdModePAL:
	{
		timecodeformat = 25;
		pic_w = PICSDWIDTH;
		pic_h = PICSDPALHEIGHT;
		if (m_sd_ratio == Mode_16_9)
		{
			pic_h = 404;
			pic_w = 720;
		}
		modeOut = ENCODER_VIDEO_FORMAT_SD_PAL;
		if (m_desinterlaceType == BOB_DEINTERLACE)
		{
			bob = true;
			timecodeformat = 50;
		}
		break;
	}
	case bmdModeNTSC:
	{
		timecodeformat = 29;
		pic_w = PICSDWIDTH;
		pic_h = PICSDNTSCHEIGHT;
		if (m_sd_ratio == Mode_16_9)
		{
			pic_h = 404;
			pic_w = 720;
		}
		if (m_desinterlaceType == BOB_DEINTERLACE)
		{
			bob = true;
			timecodeformat = 59;
		}
		modeOut = ENCODER_VIDEO_FORMAT_SD_NTSC;
		break;
	}
	}
	return modeInfo;
}

#define LINE_SIZE_1920_1080	(1920*2)
void desinterlace(unsigned char * buffVideo)
{
	for (int line = 0; line < (1080 - 2); line += 2)
	{
		unsigned char * pos = &buffVideo[(line + 1)*LINE_SIZE_1920_1080];

		for (int i = 0; i < 1920; i++)
		{
			unsigned long v = (pos[-LINE_SIZE_1920_1080] + pos[LINE_SIZE_1920_1080]) / 2;
			*pos++ = (unsigned char)v;
			v = (pos[-LINE_SIZE_1920_1080] + pos[LINE_SIZE_1920_1080]) / 2;
			*pos++ = (unsigned char)v;
		}
	}
}


HRESULT STDMETHODCALLTYPE CRecorderCore2::VideoInputFrameArrived(IDeckLinkVideoInputFrame *videoFrame, IDeckLinkAudioInputPacket *audioPacket)
{

	if (!videoFrame)
		return S_OK; // It's possible to receive a NULL inputFrame, but a valid audioPacket. Ignore audio-only frame.
	EnterCriticalSection(&m_Lock);
	if (!isSupportedFormat())
	{
		LeaveCriticalSection(&m_Lock);
		return 0;
	}

	VideoModeInfo modeInfo = GetTargetModeInfo(m_displayMode);

	if (!m_bIsVideoDropped && modeInfo.DropframeNeeded)
	{
		m_bIsVideoDropped = true;
		//LogRec::logd("Drop the video of one frame ,frameID %d ,targe mode 0x%x ,deinterlace %d, scaleto %d  ", m_nbFrameReceived , m_desinterlaceType, m_scaleTo);
	}
	else
		m_bIsVideoDropped = false;
	if (isValidVideo(videoFrame))
	{

		int v_w = videoFrame->GetWidth();
		int v_h = videoFrame->GetHeight();
		if (m_displayMode == bmdModeNTSC) //patch ,from board for NTSC is 486,but it should be 480
			v_h = 480;

		void *bufferVideo = NULL;
		videoFrame->GetBytes(&bufferVideo);		
		recordFrame(bufferVideo, 1920, modeInfo.width, modeInfo.height);

	}


	LeaveCriticalSection(&m_Lock);
	return 0;
}

#define orgWhite_Y  0X80
#define orgWhite_U  0XF0
#define orgWhite_V  0XF0
#define closeValue 5

#define isWhite_Y(x) ((orgWhite_Y-(x)&0xFF )<closeValue)
#define isWhite_U(x) ((orgWhite_U-((x)>>8)&0xFF )<closeValue)
#define isWhite_V(x) ((orgWhite_V-((x)>>24)&0xFF )<closeValue)

#define isWhite(x)  (isWhite_Y(x)&&isWhite_V(x)&&isWhite_U(x))

void  getmaker(byte *pUYVY, UINT64 &fieldTop, UINT64 &fieldBottom)
{
	int *pPosA = (int *)pUYVY;
	int *pPosB = (int *)(pUYVY + 1920 * 2);
	int nWidth = 512 / 8;
	fieldTop = 0;
	fieldBottom = 0;

	for (int i = 0; i < nWidth; i++)
	{
		if (isWhite(*pPosA))
		{
			fieldTop |= (1 << (nWidth - 1 - i));
		}
		pPosA += 8;

		if (isWhite(*pPosB))
		{

			fieldBottom |= (1 << (nWidth - 1 - i));
		}
		pPosB += 8;
	}
	//printf("\nA%8.8I64x_B%8.8I64x\n", fieldTop, fieldBottom);
}
void CRecorderCore2::recordFrame(void * _bufferVideo, long _sampleCount, int width, int height)
{	
	UINT64 fieldA=0, fieldB=0;
	getmaker((byte *)_bufferVideo, fieldA, fieldB);

	printf("\n%8.8d,A%8.8I64x_B%8.8I64x", m_nbFrameSentToDisk, fieldA, fieldB);
	
	if (g_bSaveYUV)
	{
		fprintf(g_fp_text, "%8.8d,A%8.8I64x_B%8.8I64x\n",m_nbFrameSentToDisk, fieldA, fieldB);
		fflush(g_fp_text);
		char videoHdrPath[200];
		int m_cnl = 0;
		sprintf_s(videoHdrPath, 200, "%s%d_%8.8d_A%8.8I64x_B%8.8I64x.YUV", g_yuvSavePath, m_cnl, m_nbFrameSentToDisk,fieldA,fieldB);
		FILE * fp = g_fp;// fopen(videoHdrPath, "wb");
		if (fp)
		{
			fwrite(_bufferVideo, width * height * 2, 1, fp);
			if (fp != g_fp)
			{
				fclose(fp);
				fp = NULL;
			}
		}
	}
	m_nbFrameSentToDisk++;
}
HRESULT STDMETHODCALLTYPE CRecorderCore2::QueryInterface(REFIID riid, void **ppvObject)
{
	return 0;
}

ULONG STDMETHODCALLTYPE CRecorderCore2::AddRef(void)
{
	return 0;
}

ULONG STDMETHODCALLTYPE CRecorderCore2::Release(void)
{
	return 0;
}


int CRecorder2::start()
{
	int t1 = GetTickCount();

	//m_recorderCore.loadPCTimecode();

	m_deckLinkInput = m_deckLinkInputProvider.getInput(m_cnl);
	m_deckLinkInput->SetVideoInputFrameMemoryAllocator((IDeckLinkMemoryAllocator*)&m_decklinkMemoryMgr);
	m_recorderCore->start(m_deckLinkInput, m_cnl);

	return 0;
}
void outerror(HRESULT hresult,LPCTSTR lpstrError)
{
	_tprintf(L"[%x]--%s", hresult, lpstrError);
// 	CString pouterror;
// 	pouterror.Format(_T("[%x]--%s"), hresult, lpstrError);
// 	AfxMessageBox(pouterror);
}
HRESULT CRecorderCore2::start(IDeckLinkInput * _deckLinkInput, int _cnl)
{

	m_deckLinkInput = _deckLinkInput;
	m_displayMode = bmdModeHD1080i50;
	m_isDrop = false;
	m_nbFrameReceived = 0;
	m_captureEnabled = false;
	m_cnl = _cnl;

	HRESULT result = m_deckLinkInput->EnableVideoInput(m_displayMode, bmdFormat8BitYUV, bmdVideoInputFlagDefault);		// Mini Recorder doesn't support Format Detection

	if (result != S_OK)
	{
		outerror(result,L"m_deckLinkInput->EnableVideoInput\n");
		return result;
	}

	result = m_deckLinkInput->EnableAudioInput(bmdAudioSampleRate48kHz, bmdAudioSampleType32bitInteger, 16);
	if (result != S_OK)
	{
		outerror(result, L"m_deckLinkInput->EnableAudioInput\n");
		return result;
	}

	result = m_deckLinkInput->SetCallback(this);	
	if (result != S_OK)
	{
		outerror(result, L"m_deckLinkInput->SetCallback\n");
		return result;
	}
	result = m_deckLinkInput->StartStreams();

	if (result != S_OK)
	{
		outerror(result, L"m_deckLinkInput->StartStreams\n");
		return result;
	}
	outerror(result, L"OK\n");

}
