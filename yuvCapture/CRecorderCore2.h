#pragma once
#include "DeckLinkAPI_h.h"
#include "decklinkMemoryMgr.h"
#include "deckLinkInputProvider.h"
#include <memory>
//#include "recorderCore.h"

typedef enum
{

	ENCODER_VIDEO_FORMAT_UNKOWN,
	ENCODER_VIDEO_FORMAT_SD_PAL,
	ENCODER_VIDEO_FORMAT_SD_NTSC,
	ENCODER_VIDEO_FORMAT_720p_2398,
	ENCODER_VIDEO_FORMAT_720p_24,
	ENCODER_VIDEO_FORMAT_720p_25,
	ENCODER_VIDEO_FORMAT_720p_2997,
	ENCODER_VIDEO_FORMAT_720p_30,
	ENCODER_VIDEO_FORMAT_720p_50,
	ENCODER_VIDEO_FORMAT_720p_5994,
	ENCODER_VIDEO_FORMAT_720p_60,
	ENCODER_VIDEO_FORMAT_1080i_50,
	ENCODER_VIDEO_FORMAT_1080i_5994,
	ENCODER_VIDEO_FORMAT_1080i_60,
	ENCODER_VIDEO_FORMAT_1080p_2398,
	ENCODER_VIDEO_FORMAT_1080p_24,
	ENCODER_VIDEO_FORMAT_1080p_25,
	ENCODER_VIDEO_FORMAT_1080p_2997,
	ENCODER_VIDEO_FORMAT_1080p_30,
	ENCODER_VIDEO_FORMAT_1080p_5994
}VideoFormat_ENUM;

#define PIC1080HEIGHT 1080
#define PIC1080WIDTH  1920
#define PIC720HEIGHT  720
#define PIC720WIDTH   1280
#define PICSDWIDTH 720
#define PICSDPALHEIGHT 576
#define PICSDNTSCHEIGHT 480
#define PICSD404HEIGHT 404

enum  MMR_SDOutputMode
{
	Mode_4_3 = 0x0,
	Mode_16_9,
	Mode_Unknown = 0xffffffff,
};

typedef struct _VideoModeInfo
{
	VideoFormat_ENUM mode;
	int width;
	int height;
	bool BobNeeded;
	bool DropframeNeeded;
	int  timecodeformat;
	_VideoModeInfo()
	{
		mode = ENCODER_VIDEO_FORMAT_UNKOWN;
		width = 0;
		height = 0;
		BobNeeded = false;
		DropframeNeeded = false;
		timecodeformat = 0;
	}
}VideoModeInfo;

class CRecorderCore2 : public IDeckLinkInputCallback
{
	IDeckLinkInput		* m_deckLinkInput;
	BMDDisplayMode	      m_displayMode;
	bool                  m_isDrop;
	volatile bool m_captureEnabled;
	unsigned long m_nbFrameReceived;
	int m_cnl;
	bool m_bIsVideoDropped = false;
	CRITICAL_SECTION	  m_Lock;

	unsigned long		  m_scaleTo;
	unsigned long		  m_dumpUncompressed;
	unsigned long		  m_sd_ratio;
	int                   m_desinterlaceType = 1;
	unsigned long		  m_nbFrameSentToDisk = 0;
public:
	CRecorderCore2();
	~CRecorderCore2();
	bool isSupportedFormat();
	bool isValidVideo(IDeckLinkVideoInputFrame * _videoFrame);


	VideoModeInfo GetTargetModeInfo(int modeIn);
	virtual HRESULT STDMETHODCALLTYPE VideoInputFormatChanged(BMDVideoInputFormatChangedEvents notificationEvents, IDeckLinkDisplayMode *newDisplayMode, BMDDetectedVideoInputFormatFlags detectedSignalFlags);


	virtual HRESULT STDMETHODCALLTYPE VideoInputFrameArrived(IDeckLinkVideoInputFrame *videoFrame, IDeckLinkAudioInputPacket *audioPacket);
	HRESULT CRecorderCore2::start(IDeckLinkInput * _deckLinkInput, int _cnl);

	void recordFrame(void * _bufferVideo, long _sampleCount, int width, int height);

	virtual HRESULT STDMETHODCALLTYPE QueryInterface(REFIID riid, void **ppvObject);


	virtual ULONG STDMETHODCALLTYPE AddRef(void);


	virtual ULONG STDMETHODCALLTYPE Release(void);

};
class CRecorder2
{
	IDeckLinkInput * m_deckLinkInput; 
	decklinkMemoryMgr m_decklinkMemoryMgr;
	CDeckLinkInputProvider m_deckLinkInputProvider;
	int m_cnl;
	CRecorderCore2* m_recorderCore;
public:
	CRecorder2(int _cnl)
	{
		m_cnl = _cnl;
		m_deckLinkInput = nullptr;
		m_recorderCore = new CRecorderCore2();
		m_decklinkMemoryMgr.start();
	}
	int CRecorder2::start();
};

