#include "CRecorderCore2.h"


int main_recoder2(int argc, char * argv[])
{
	//return main_recoder(argc, argv);
	printf("+-----------------------------------------+\n");
	printf("+              Test Recorder2              +\n");
	printf("+-----------------------------------------+\n");

	int cnl = (argc > 1) ? atoi(argv[1]) : 0;

	cnl = 3;
	printf("Cnl %d\n", cnl);

	CRecorder2 recorder(cnl);
	recorder.start();

	printf("Press q key to exit\n");
	while (1)
	{
		char a = getchar();

		if (a == 'q')
		{
			//recorder.stop();
			return 0;
		}
	}

	return 0;
}

void writeMarker(byte*pUYVY, UINT64 nFrameNum)
{
	UINT64 nFieldA = nFrameNum * 2 + 1;
	UINT64 nFieldB = nFieldA + 1;
	//printf("\n frame:%I64d,A::%I64d B:%I64d\n", nFrameNum, nFieldA, nFieldB);
	{

		byte*pPos1 = pUYVY;
		byte*pPos2 = pUYVY + 1920 * 2;
		int nWidth = 512 / 8;
		//for (int j = 0; j < 8; j++)
		{
			int *pPosA = (int *)pPos1;
			int *pPosB = (int *)pPos2;
			printf("W:\n");
			for (int i = 0; i < nWidth; i++)
			{
				bool bWriteWhiteA = false;

				bWriteWhiteA = (nFieldA >> (nWidth - 1 - i)) & 1;
				printf("%d", bWriteWhiteA);
				for (int ii = 0; ii < 8; ii++)
				{
					*pPosA++ = (bWriteWhiteA) ? 0xF080f080 : 0x10801080;
				}
			}

			printf("\tWA:%d\n", nFieldA);
			for (int i = 0; i < nWidth; i++)
			{
				bool bWriteWhiteB = false;

				bWriteWhiteB = (nFieldB >> (nWidth - 1 - i)) & 1;
				printf("%d",bWriteWhiteB);
				for (int ii = 0; ii < 8; ii++)
				{
					*pPosB++ = (bWriteWhiteB) ? 0xF080f080 : 0x10801080;
				}
			}
			printf("\tWB:%d\n", nFieldB);
			pPos1 += 1920 * 4;
			pPos2 += 1920 * 4;
		}
	}
	//writeMarker2(pUYVY, nFrameNum, 16 * 50);
}

#define orgWhite_Y  0X80
#define orgWhite_U  0XF0
#define orgWhite_V  0XF0
#define closeValue 5

#define isWhite_Y(x) ((orgWhite_Y-(x)&0xFF )<closeValue)
#define isWhite_U(x) ((orgWhite_U-((x)>>8)&0xFF )<closeValue)
#define isWhite_V(x) ((orgWhite_V-((x)>>24)&0xFF )<closeValue)

#define isWhite(x)  (isWhite_Y(x)&&isWhite_V(x)&&isWhite_U(x))


void  getmaker2(byte *pUYVY, UINT64 &fieldTop, UINT64 &fieldBottom)
{
	int *pPosA = (int *)pUYVY;
	int *pPosB = (int *)(pUYVY + 1920 * 2);
	int nWidth = 512 / 8;
	fieldTop = 0;
	fieldBottom = 0;
	
	for (int i = 0; i < nWidth; i++)
	{
		if (isWhite(*pPosB))
		{

			fieldBottom |= (1 << (nWidth - 1 - i));
		}
		pPosB += 8;

		if (isWhite(*pPosA))
		{
			fieldTop |= (1 << (nWidth - 1 - i));
		}
		pPosA += 8;


	}
}

void  getmaker(byte *pUYVY,int nFrames)
{
	UINT64 fieldTop, fieldBottom;
	getmaker2(pUYVY, fieldTop, fieldBottom);
	printf("\nA%8.8I64x_B%8.8I64x\n", fieldTop, fieldBottom);
} 
int testMark()
{
	byte *pBUffer = new byte[1920 * 1080 * 2];
	ZeroMemory(pBUffer, 1920 * 1080 * 2);

	
		FILE *fp;
		fopen_s(&fp, "d:\\data\\global.yuv", "rb");
		//fopen_s(&fp, "d:\\data\\global_z.yuv", "rb");
		int nsize = 1920 * 1080 * 2;
		unsigned char*pYUV = new unsigned char[nsize];
		int icount = 0;
		while (1)
		{
			int nRead = fread(pYUV, 1, nsize, fp);
			if (nRead != nsize)break;
			//if (icount++<27)
			//	continue;
			
			getmaker(pYUV, 0);
			//if(icount>40)
				break;
		}
		fclose(fp);


		getchar();
	for (int i = 0; i < 10; i++)
	{
		writeMarker(pBUffer, i);
		getmaker(pBUffer, i);
	}

	return 0;

}
int main(int argc, char * argv[])
{
	//return	testMark();
// 	//getchar();
	return main_recoder2(argc, argv);

}