#pragma once

#include <atlbase.h>

//-----------------------------------------------------------------------------
#define MAX_CAM 4
class CRegistryEdit
{
	HKEY m_hk;
	HKEY m_hKCam[MAX_CAM];

	void Init();
public:
	CRegistryEdit();
	~CRegistryEdit();

	int getVal(LPTSTR id, unsigned long *val, unsigned long defVal);
	int setVal(LPTSTR id, unsigned long val);
	int getValCamVal(int cnlID, LPTSTR id, unsigned long *val, unsigned long defVal);
	int setCamVal(int cnlID, LPTSTR id, unsigned long val);
	int getCameraValString(LPCSTR id, LPBYTE val, unsigned long valSize, LPBYTE defVal, unsigned char cameraIndex);
	int setCameraValString(LPCSTR id, LPBYTE val, unsigned char cameraIndex);
};

//-----------------------------------------------------------------------------


