#include "COMbase.h"

//-----------------------------------------------------------------------------

CCOMbase::~CCOMbase()
{
	if (m_bInitDone)
		CoUninitialize();	// Uninitalize COM on this thread
}

//-----------------------------------------------------------------------------

HRESULT CCOMbase::init()
{
	HRESULT result = CoInitialize(NULL);	// Initialize COM on this thread
	if (FAILED(result))
		return result;
	m_bInitDone = true;	

	result =  CoInitializeSecurity(
        NULL, 
        -1,                          // COM authentication
        NULL,                        // Authentication services
        NULL,                        // Reserved
        RPC_C_AUTHN_LEVEL_DEFAULT,   // Default authentication 
        RPC_C_IMP_LEVEL_IMPERSONATE, // Default Impersonation  
        NULL,                        // Authentication info
        EOAC_NONE,                   // Additional capabilities 
        NULL                         // Reserved
        );

	return result;
}

//-----------------------------------------------------------------------------
