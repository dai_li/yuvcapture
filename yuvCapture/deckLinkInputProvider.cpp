#include "deckLinkInputProvider.h"

//------------------------------------------------------------------------

CDeckLinkInputProvider::CDeckLinkInputProvider()
{
	m_deckLinkIterator = NULL;
	m_deckLink = NULL;

	COMbase.init();
}

//------------------------------------------------------------------------

CDeckLinkInputProvider::~CDeckLinkInputProvider()
{
	release();
}

//------------------------------------------------------------------------

void CDeckLinkInputProvider::release()
{
	if (m_deckLinkInput)
	{
		m_deckLinkInput->Release();
		m_deckLinkInput = NULL;
	}
	if (m_deckLink)
	{
		m_deckLink->Release();
		m_deckLink = NULL;
	}
	if (m_deckLinkIterator)
	{
		m_deckLinkIterator->Release();
		m_deckLinkIterator = NULL;
	}
}

//------------------------------------------------------------------------

IDeckLinkInput * CDeckLinkInputProvider::getInput(int _cnl)
{
	HRESULT result = 0;

	result = CoCreateInstance(CLSID_CDeckLinkIterator, NULL, CLSCTX_ALL, IID_IDeckLinkIterator, (void**)&m_deckLinkIterator);
	if (FAILED(result))
		return NULL;

	//printf("getInput( %d )\n", _cnl);
	//unsigned long proto = 0;
	//m_registryEdit.getVal(TEXT("Proto"), &proto, 0);

	//if (proto == 1)		// we need to swap the input 0 and 1 when we have mini Recorder
	//{
	//	if (_cnl == 1)
	//		_cnl = 0;
	//	else if (_cnl == 0)
	//		_cnl = 1;
	//}
	unsigned long channelNum = 0;
	TCHAR name[20];
	_stprintf_s(name, L"CoreRec%d", _cnl);
	m_registryEdit.getVal(name, &channelNum, _cnl);
	_cnl = channelNum;
	printf("getInput( %d )\n", _cnl);

	int cp = -1;
	do
	{
		result = m_deckLinkIterator->Next(&m_deckLink);
		cp++;
		if ((cp != _cnl) && (result == S_OK))
			m_deckLink->Release();
	} while ((result == S_OK) && (cp != _cnl));						// Enumerate all cards in this system

	result = m_deckLink->QueryInterface(IID_IDeckLinkInput, (void**)&m_deckLinkInput);
	if (FAILED(result))
	{
		m_deckLinkIterator->Release();
		m_deckLinkIterator = NULL;
	}

	return m_deckLinkInput;
}

//------------------------------------------------------------------------