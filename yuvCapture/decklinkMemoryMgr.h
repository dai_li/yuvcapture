#pragma once

#include "DeckLinkAPI_h.h"

#define MAX_FRAME_IN_MEM	71

class decklinkMemoryMgr : IDeckLinkMemoryAllocator
{
	void * m_buffer[MAX_FRAME_IN_MEM];
	bool   m_buffUsed[MAX_FRAME_IN_MEM];
	int    m_maxBufUsed;

public:
	decklinkMemoryMgr();
	~decklinkMemoryMgr();

	void start();
	void stop();

	HRESULT STDMETHODCALLTYPE AllocateBuffer(unsigned long bufferSize,void **allocatedBuffer);
	HRESULT STDMETHODCALLTYPE ReleaseBuffer(void *buffer);
	HRESULT STDMETHODCALLTYPE Commit(void);
	HRESULT STDMETHODCALLTYPE Decommit(void);

	// IUnknown needs only a dummy implementation
	virtual HRESULT	STDMETHODCALLTYPE	QueryInterface(REFIID iid, LPVOID *ppv)	{ return E_NOINTERFACE; }
	virtual ULONG	STDMETHODCALLTYPE	AddRef()									{ return 1; }
	virtual ULONG	STDMETHODCALLTYPE	Release()									{ return 1; }


};