#include "registryEdit.h"
#include <windows.h>

//-----------------------------------------------------------------------------

CRegistryEdit::CRegistryEdit()
{
	m_hk = NULL;

	long ret = RegOpenKeyEx(HKEY_CURRENT_USER, TEXT("SOFTWARE\\Id4.tv\\MMR410"), 0, KEY_SET_VALUE|KEY_QUERY_VALUE, &m_hk);
	if (ret == 2)
	{
		DWORD dwDisposition;
		ret = RegCreateKeyEx(HKEY_CURRENT_USER, TEXT("SOFTWARE\\Id4.tv\\MMR410"), 0, NULL, 0, 0, NULL, &m_hk, &dwDisposition);
		ret = RegOpenKeyEx(HKEY_CURRENT_USER, TEXT("SOFTWARE\\Id4.tv\\MMR410"), 0, KEY_SET_VALUE|KEY_QUERY_VALUE, &m_hk);
	}

	Init();


}



void CRegistryEdit::Init()
{
	long ret = 0;
	for (int index = 0; index < MAX_CAM; index++)
	{
		TCHAR  p[256];
		_stprintf(p, L"SOFTWARE\\Id4.tv\\MMR410\\Cam%d", index);
		ret = RegOpenKeyEx(HKEY_CURRENT_USER, p, 0, KEY_SET_VALUE | KEY_QUERY_VALUE, &m_hKCam[index]);

		if (ret == 2)
		{
			DWORD dwDisposition;
			ret = RegCreateKeyEx(HKEY_CURRENT_USER, p, 0, NULL, 0, 0, NULL, &m_hKCam[index], &dwDisposition);
			ret = RegOpenKeyEx(HKEY_CURRENT_USER, p, 0, KEY_SET_VALUE | KEY_QUERY_VALUE, &m_hKCam[index]);
		}
		/*
		setCamVal(index, TEXT("y1280offset"), 32);
		setCamVal(index, TEXT("y720offset"), 32);
		setCamVal(index, TEXT("y1080offset"), 32);
		setCamVal(index, TEXT("y576offset"), 32);
		setCamVal(index, TEXT("y480offset"), 32);

		setCamVal(index, TEXT("x720offset"), 96);
		setCamVal(index, TEXT("x1280offset"), 96);
		setCamVal(index, TEXT("x1080offset"), 160);
		setCamVal(index, TEXT("x576offset"), 160);
		setCamVal(index, TEXT("x480offset"), 160);
		*/
	}
}

//-----------------------------------------------------------------------------

CRegistryEdit::~CRegistryEdit()
{
	RegCloseKey(m_hk);
}

//-----------------------------------------------------------------------------

int CRegistryEdit::getVal(LPTSTR id, unsigned long *val, unsigned long defVal)
{
	DWORD type = REG_DWORD;
	DWORD cbData = 1024;
	int ret = RegQueryValueEx(m_hk, id, 0, &type, (LPBYTE)val, &cbData);
	if (ret != 0)
		*val = defVal;
	return ret;
}

int CRegistryEdit::getValCamVal(int cnlID, LPTSTR id, unsigned long *val, unsigned long defVal)
{
	DWORD type = REG_DWORD;
	DWORD cbData = 1024;
	int ret = RegQueryValueEx(m_hKCam[cnlID], id, 0, &type, (LPBYTE)val, &cbData);
	if (ret != 0)
		*val = defVal;
	return ret;

}

int CRegistryEdit::setCamVal(int cnlID, LPTSTR id, unsigned long val)
{
	int ret = RegSetValueEx(m_hKCam[cnlID], id, 0, REG_DWORD, (LPBYTE)&val, sizeof(DWORD));
	return ret;
}

//-----------------------------------------------------------------------------

int CRegistryEdit::setVal(LPTSTR  id, unsigned long val)
{
	int ret = RegSetValueEx(m_hk, id, 0, REG_DWORD, (LPBYTE)&val, sizeof(DWORD));
	return ret;
}
int CRegistryEdit::setCameraValString(LPCSTR id, LPBYTE val, unsigned char cameraIndex)
{
	int ret = RegSetValueExA(m_hKCam[cameraIndex], id, 0, REG_SZ, val, strlen((char *)val));
	return ret;
}
int CRegistryEdit::getCameraValString(LPCSTR id, LPBYTE val, unsigned long valSize, LPBYTE defVal, unsigned char cameraIndex)
{
	DWORD type = REG_SZ;
	int ret = RegQueryValueExA(m_hKCam[cameraIndex], id, nullptr, &type, val, &valSize);
	if (ret != 0 && defVal != nullptr)
		strcpy_s((char *)val, valSize, (char *)defVal);
	return ret;
}

//-----------------------------------------------------------------------------


