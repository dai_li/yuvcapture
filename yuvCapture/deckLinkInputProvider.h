#pragma once

#include "DeckLinkAPI_h.h"
#include "COMbase.h"

#include "registryEdit.h"

//-----------------------------------------------------------------------------

class CDeckLinkInputProvider
{
	CRegistryEdit	m_registryEdit;

	IDeckLinkIterator	* m_deckLinkIterator;
	IDeckLink			* m_deckLink;
	IDeckLinkInput		* m_deckLinkInput;

	CCOMbase	COMbase;

public:
	CDeckLinkInputProvider();
	~CDeckLinkInputProvider();

	void release();
	IDeckLinkInput * getInput(int _cnl);

};

//-----------------------------------------------------------------------------
	

