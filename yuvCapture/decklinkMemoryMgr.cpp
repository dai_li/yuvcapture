#include "decklinkMemoryMgr.h"

#include <stdio.h>
#include <smmintrin.h>


#define FRAME_BUF_SIZE	5000000

void decklinkMemoryMgr::start()
{
	m_maxBufUsed = 0;
	for (int i = 0; i < MAX_FRAME_IN_MEM; i++)
	{
		m_buffer[i] = _aligned_malloc(FRAME_BUF_SIZE, 16);
		m_buffUsed[i] = false;
	}
}

void decklinkMemoryMgr::stop()
{
	printf("m_maxBufUsed = %d\n", m_maxBufUsed);
	for (int i = 0; i < MAX_FRAME_IN_MEM; i++)
	{
		_aligned_free(m_buffer[i]);
		m_buffer[i] = NULL;
	}
}


decklinkMemoryMgr::decklinkMemoryMgr()
{
}

decklinkMemoryMgr::~decklinkMemoryMgr()
{
}


HRESULT STDMETHODCALLTYPE decklinkMemoryMgr::AllocateBuffer(unsigned long bufferSize, void **allocatedBuffer)
{
	if (bufferSize >= FRAME_BUF_SIZE)
	{
		printf("AllocateBuffer  E_OUTOFMEMORY   (%ld >= %ld)\n", bufferSize , FRAME_BUF_SIZE);
		return E_OUTOFMEMORY;
	}


	for (int i = 0; i < MAX_FRAME_IN_MEM; i++)
	{
		if (m_buffUsed[i] == false)
		{
			m_buffUsed[i] = true;
			*allocatedBuffer = m_buffer[i];
			//printf("AllocateBuffer[%d] %p (%ld)\n", i, m_buffer[i], bufferSize);
			if (i > m_maxBufUsed)
				m_maxBufUsed = i;
			return S_OK;
		}
	}

	printf("AllocateBuffer  E_OUTOFMEMORY\n");
	return E_OUTOFMEMORY;
}

HRESULT STDMETHODCALLTYPE decklinkMemoryMgr::ReleaseBuffer(void *buffer)
{
	for (int i = 0; i < MAX_FRAME_IN_MEM; i++)
	{
		if (m_buffer[i] == buffer)
		{
			//printf("ReleaseBuffer[%d] %p\n", i, buffer);
			m_buffUsed[i] = false;
			return S_OK;
		}
	}

	printf("ReleaseBuffer Error  %p\n", buffer);
	return 0;
}

HRESULT STDMETHODCALLTYPE decklinkMemoryMgr::Commit(void)
{
	return 0;
}

HRESULT STDMETHODCALLTYPE decklinkMemoryMgr::Decommit(void)
{
	return 0;
}



